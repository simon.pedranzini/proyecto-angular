import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
{path:'',loadChildren:()=>import('./pages/home/home.module').then(m=>m.HomeModule)},
{path:'inicio',loadChildren:()=>import('./pages/home/home.module').then(m=>m.HomeModule)},
{path:'registro',loadChildren:()=>import('./pages/register/register.module').then(m=>m.RegisterModule)},
{path:'personajes',loadChildren:()=>import('./pages/characters/characters.module').then(m=>m.CharactersModule)},
{path:'personajes/:heroId',loadChildren:()=>import('./pages/characters/components/hero-character/hero-character.component').then(m=>m.HeroCharacterComponent)},
{path:'personajes/:evilId',loadChildren:()=>import('./pages/characters/components/evil-character/evil-character.component').then(m=>m.EvilCharacterComponent)},
{path:'planetas',loadChildren:()=>import('./pages/planets/planets.module').then(m=>m.PlanetsModule)},
{path:'videoteca',loadChildren:()=>import('./pages/video-library/video-library.module').then(m=>m.VideoLibraryModule)},
{path:'**',redirectTo:'inicio',pathMatch:'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
