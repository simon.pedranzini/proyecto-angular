import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavBarModule } from './components/nav-bar/nav-bar.module';
import { LoaderComponent } from './components/loader/loader.component';
import { BackModule } from './components/back/back.module';



@NgModule({
  declarations: [
    LoaderComponent,

  ],
  imports: [
    CommonModule
  ],
  exports: [NavBarModule,LoaderComponent,BackModule]
})
export class SharedModule { }
