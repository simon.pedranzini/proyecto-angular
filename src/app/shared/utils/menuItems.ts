export interface MenuI {
    label: string,
    url: string,
    img: string
}