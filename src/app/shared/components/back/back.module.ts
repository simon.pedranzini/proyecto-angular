import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CharactersRoutingModule } from 'src/app/pages/characters/characters-routing.module';
import { BackComponent } from './back.component';


@NgModule({
  declarations: [BackComponent],
  imports: [
    CommonModule,
    CharactersRoutingModule,
  ],

  exports: [BackComponent,BackComponent]
})
export class BackModule { }
