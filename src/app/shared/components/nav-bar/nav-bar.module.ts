import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from 'src/app/pages/home/home-routing.module';
import { NavBarComponent } from './nav-bar.component';

@NgModule({
  declarations: [NavBarComponent],
  imports: [
    CommonModule,
    HomeRoutingModule
  ],
  exports: [NavBarComponent]
})
export class NavBarModule { }
