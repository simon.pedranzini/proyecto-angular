import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CharactersI } from 'src/app/models/dragonBall';

@Injectable()

export class CharactersService {

  constructor(private http: HttpClient) { }

    getAllCharacters(): Observable<CharactersI> {
      return this.http.get<CharactersI>('http://localhost:3000/characters');
    }

}
