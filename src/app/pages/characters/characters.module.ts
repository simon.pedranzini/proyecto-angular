//Modulos
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CharactersRoutingModule } from './characters-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { HttpClientModule } from '@angular/common/http'
//Componentes
import { CharactersComponent } from './characters.component';
//Servicios
import { CharactersService } from './characters.service';
import { HeroCharacterComponent } from './components/hero-character/hero-character.component';
import { EvilCharacterComponent } from './components/evil-character/evil-character.component';
import { CharactersDetailComponent } from './components/characters-detail/characters-detail.component';

@NgModule({
  declarations: [
    CharactersComponent,
    HeroCharacterComponent,
    EvilCharacterComponent,
    CharactersDetailComponent
  ],
  imports: [
    CommonModule,
    CharactersRoutingModule,
    SharedModule, //El Navbar
    HttpClientModule //Para la petición GET del HTTP
  ],
  providers: [CharactersService], //Importamos el servicio solo para utilizarlo aquí
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CharactersModule { }
