import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { CharactersI } from 'src/app/models/dragonBall';
import { CharactersService } from '../../characters.service';

@Component({
  selector: 'app-characters-detail',
  templateUrl: './characters-detail.component.html',
  styleUrls: ['./characters-detail.component.scss']
})
export class CharactersDetailComponent implements OnInit {
  evilCharacter:string
  charactersList:any=[]
  characterDetail?: CharactersI ={
    id: '',
    name: '',
    race: '',
    gender: '',
    type: '',
    bio: '',
    abilities:[],
    img: '',
}

paramMapSubsciption?:Subscription
  constructor(private routes:ActivatedRoute,private apiService:CharactersService) { }

  ngOnInit(): void {
    this.paramMapSubsciption= this.routes.paramMap.subscribe(data=>{
      this.apiService.getAllCharacters().subscribe((datos) => {
       this.charactersList = datos;
       this.paramMapSubsciption= this.routes.paramMap.subscribe((params) => {
        const characterId = params.get('characterDetail');
        this.characterDetail = this.charactersList.find(
          (character:any) => character.id == characterId
        )!;
     })})
    })}}
