import { ActivatedRoute } from '@angular/router';
import { Component, Input, OnInit } from '@angular/core';
import { CharactersI } from 'src/app/models/dragonBall';
@Component({
  selector: 'app-hero-character',
  templateUrl: './hero-character.component.html',
  styleUrls: ['./hero-character.component.scss']
})
export class HeroCharacterComponent implements OnInit {

  @Input() hero: CharactersI;
  public heroCharacter: string;

  constructor( private routes: ActivatedRoute) { }

  ngOnInit(): void {
  }

}
