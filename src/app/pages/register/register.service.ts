import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { WarriorsI } from 'src/app/models/dragonBall';

@Injectable()
export class RegisterService {

  public apiRegister = 'http://localhost:3000/warriors'

  constructor(private http: HttpClient) { }

  showWarriors(): Observable<WarriorsI> {
    return this.http.get<WarriorsI>(this.apiRegister)
  }

  addwarrior(warrior: WarriorsI): Observable<any> {
    return this.http.post<any>(this.apiRegister, warrior)
  }
}
