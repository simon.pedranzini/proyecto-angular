import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { VideoLibraryI } from "src/app/models/dragonBall";

@Injectable()

export class VideosService{

  constructor(private http: HttpClient) { }

  getAllVideos(): Observable<VideoLibraryI>{
    return this.http.get<VideoLibraryI>('http://localhost:3000/video_library')
  }
}
