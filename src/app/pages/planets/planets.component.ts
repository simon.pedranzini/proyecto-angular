import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { PlanetsService } from './planets.service';

@Component({
  selector: 'app-planets',
  templateUrl: './planets.component.html',
  styleUrls: ['./planets.component.scss']
})
export class PlanetsComponent implements OnInit {

  public planetsList : any = []
  private planetsSubscriptions$?: Subscription;

  constructor(private allPlanetsSrv: PlanetsService) { }

  ngOnInit(): void {
    this.printAllCharacters();
  }
  printAllCharacters(){
    this.planetsSubscriptions$= this.allPlanetsSrv.getAllPlanets().subscribe((data)=>{
      this.planetsList= data;
    })
  }

  ngOnDestroy(): void {
    this.planetsSubscriptions$?.unsubscribe();
  }
}
