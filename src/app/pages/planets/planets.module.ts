
//Modulos
import { CUSTOM_ELEMENTS_SCHEMA} from'@angular/core';
// import { NavBarComponent } from './../../shared/components/nav-bar/nav-bar.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlanetsRoutingModule } from './planets-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
//Componentes
import { PlanetsComponent } from './planets.component';
//servicios
import { PlanetsService } from './planets.service';
import { PlanetDetailComponent } from './planet-detail/planet-detail.component';



@NgModule({
  declarations: [
    PlanetsComponent,
    PlanetDetailComponent,
  ],
  imports: [
    CommonModule,
    PlanetsRoutingModule,
    SharedModule,
    HttpClientModule,
  ],
  providers: [PlanetsService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PlanetsModule { }
