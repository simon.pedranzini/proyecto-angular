import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { PlanetsI } from "src/app/models/dragonBall";

@Injectable()

export class PlanetsService{

  public urlPlanets = 'http://localhost:3000/planets'

  constructor(private http: HttpClient) { }

  getAllPlanets(): Observable<PlanetsI>{
    return this.http.get<PlanetsI>(this.urlPlanets)
  }

  getPlanetById(id: string): Observable<any> {
    return this.http.get(this.urlPlanets + '/' + id)
  }

}
