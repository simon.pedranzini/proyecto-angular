import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { PlanetsService } from '../planets.service';

@Component({
  selector: 'app-planet-detail',
  templateUrl: './planet-detail.component.html',
  styleUrls: ['./planet-detail.component.scss']
})
export class PlanetDetailComponent implements OnInit {

  public planetId : string;
  public listado: any;
  public planetDetail : any = {};
  paramMapSubscription$: Subscription;

  constructor(private routes: ActivatedRoute,
              private allPlanetsSrv: PlanetsService) { }

  ngOnInit(): void {
    this.paramMapSubscription$ = this.routes.paramMap //PARAMMAP, listado de parametros
        .subscribe(data => {
          this.planetId = data.get("planetId") || ''; //productId el que recibe por url, del path routing.
          // console.log(this.planetId)
          // this.planetDetail =
        });

  this.allPlanetsSrv.getPlanetById(this.planetId).subscribe((data) => {
    this.planetDetail = data;
  })
}
}
