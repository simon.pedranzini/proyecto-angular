# Proyecto Angular

Este proyecto esta generado con [Angular CLI](https://github.com/angular/angular-cli) version 13.0.4.

## Servidor desarollo

Ejecuta `ng serve` para ponerlo en desarollo. Navega hasta `http://localhost:4200/`. La aplicación automáticamente se cargara si haces algún cambio en los archivos.

## Dependencias necesarias:

Bootstrap => npm install bootstrap jquery @popperjs/core

## Servidor DDBB desarrollo

Para poder utilizar el fichero en server/db.json es necesario ejecutar nuestra base de datos. Lo haremos a través de JSON-SERVER Fake API con el siguiente comando:
Instalación(solo una vez): npm install -g json-server
Ejecucion(cada vez que se abra el proyecto): npm run serverAPI